package com.lsh.util;

import lombok.Data;

import java.util.List;

/**
 * 分页结果类
 */
@Data
public class PageResult<T> {
    private long total;   // 总条数
    private List<T> rows; // 所有结果

    public PageResult(long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }
}
