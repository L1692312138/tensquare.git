package com.lsh.service;

import com.lsh.model.*;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:53 下午
 * @desc ：
 */
public interface SysService {

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    SysUser findUserByUsername(String username);

    /**
     * 根据用户ID 查询角色信息ID
     * @param id
     * @return
     */
    List<SysUserRole> findRolesIdByUserID(Integer id);

    /**
     * 根据角色ID 查询角色
     * @param roleId
     * @return
     */
    SysRole findRoleByRoleId(Integer roleId);

    /**
     * 根据角色name查询 角色信息
     * @param roleName
     * @return
     */
    SysRole findRoleIdBuRoleName(String roleName);


    /**
     * 根绝角色ID查询权限ID
     * @param id
     * @return
     */
    List<SysRolePermission> findSysRolePermissionByRoleId(Integer id);

    /**
     * 根据权限id查询权限
     * @param permissionId
     * @return
     */
    SysPermission findPermissionByPermessionId(Integer permissionId);


    /**
     * 添加用户
     * @param sysUser
     */
    void addSysUser(SysUser sysUser);

}
