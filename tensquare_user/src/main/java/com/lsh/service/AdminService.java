package com.lsh.service;

import com.lsh.model.Admin;
import com.lsh.util.ResultObject;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/3 1:43 下午
 * @desc ：
 */
public interface AdminService {
    void addAdmin(Admin admin);

    Admin login(Admin admin);

    ResultObject deleteById(String id);
}
