package com.lsh.service;

import com.lsh.model.User;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:01 下午
 * @desc ：
 */
public interface UserService {
    List<User> findAll();
    void toRabbitMQ();


}
