package com.lsh.service.Impl;

import com.lsh.model.User;
import com.lsh.repository.UserRepository;
import com.lsh.service.UserService;
import com.lsh.util.IdWorker;
import com.lsh.repository.AdminRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:02 下午
 * @desc ：
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    IdWorker idWorker;
    /**
     * 加解密
     */
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void toRabbitMQ() {
        rabbitTemplate.convertAndSend("","","");
    }


}
