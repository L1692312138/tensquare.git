package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:49 下午
 * @desc ：
 */
@Data
@Entity(name = "sys_permission")
public class SysPermission {
    @Id
    private Integer id;
    private String url;
    private String permission;
}
