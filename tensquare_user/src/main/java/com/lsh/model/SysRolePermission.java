package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:50 下午
 * @desc ：
 */
@Data
@Entity(name = "sys_role_permission")
public class SysRolePermission {
    @Id
    private Integer id;
    private Integer roleId;
    private Integer permissionId;
}