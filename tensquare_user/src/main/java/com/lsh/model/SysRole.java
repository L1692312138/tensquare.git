package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 2:48 下午
 * @desc ：
 */
@Data
@Entity(name = "sys_role")
public class SysRole {
    @Id
    private Integer id;
    private String name;
}

