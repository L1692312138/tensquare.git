package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/3 10:48 上午
 * @desc ：
 */
@Data
@Entity
@Table(name="tb_admin")
public class Admin implements Serializable {

    /**
     * ID
     */
    @Id
    private String id;
    /**
     * 登陆名称
     */
    private String loginname;
    /**
     * 密码
     */
    private String password;
    /**
     * 状态
     */
    private String state;
}
