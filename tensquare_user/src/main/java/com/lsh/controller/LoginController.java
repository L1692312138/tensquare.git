package com.lsh.controller;

import com.lsh.model.SysUser;
import com.lsh.service.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/9 1:54 下午
 * @desc ：
 */
@Controller
public class LoginController {
    @Autowired
    SysService sysService;
    @Autowired
    private SessionRegistry sessionRegistry ;


    @GetMapping("/")
    public String showHome() {
        /**
         * 从session中获取登录信息,判断有无登录
         */
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println("/方法执行");
        System.out.println("name:"+name);
        return "home";
    }

    /**
     * 跳转至登录页面
     * @return
     */
    @GetMapping("/login")
    public String showLogin() {
        System.out.println("/login方法执行");
        return "index";
    }

    /**
     * 访问/admin需要有ROLE_ADMIN角色
     * @return
     */
    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
    public String printAdmin() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色";
    }

    /**
     * 访问/user需要有ROLE_USER角色
     * @return
     */
    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_USER')")
    @ResponseBody
    public String printUser() {
        return "如果你看见这句话，说明你有ROLE_USER角色";
    }

    /**
     * 访问/anno不需要权限
     * @return
     */
    @GetMapping("/anon")
    @ResponseBody
    public String anon() {
        return "如果你看见这句话，说明你在匿名访问";
    }

    /**
     * 访问/adminR 需要admin角色 并且需要c权限
     * @return
     */
    @GetMapping("/adminC")
    @ResponseBody
    @PreAuthorize("hasPermission('/admin','c')")
    public String printAdminC() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色且具有c权限";
    }
    /**
     * 访问/adminR 需要admin角色 并且需要r权限
     * @return
     */
    @GetMapping("/adminR")
    @ResponseBody
    @PreAuthorize("hasPermission('/admin','r')")
    public String printAdminR() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色且具有r权限";
    }
    /**
     * 访问/adminR 需要admin角色 并且需要r权限
     * @return
     */
    @GetMapping("/adminU")
    @ResponseBody
    @PreAuthorize("hasPermission('/admin','u')")
    public String printAdminU() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色且具有u权限";
    }
    /**
     * 访问/adminR 需要admin角色 并且需要r权限
     * @return
     */
    @GetMapping("/adminD")
    @ResponseBody
    @PreAuthorize("hasPermission('/admin','d')")
    public String printAdminD() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色且具有d权限";
    }

    /**
     * 访问/userR 需要user角色 并且需要r权限
     * @return
     */
    @GetMapping("/userR")
    @PreAuthorize("hasPermission('/user','r')")
    @ResponseBody
    public String printUser3() {
        return "如果你看见这句话，说明你有ROLE_USER角色且具有r权限";
    }

    @GetMapping("/user/C")
    @PreAuthorize("hasPermission('/user','c')")
    @ResponseBody
    public String printUser2() {
        return "如果你看见这句话，说明你有ROLE_USER角色且具有c权限";
    }



    @PostMapping("/addSysUser")
    @ResponseBody
    public String addSysUser(SysUser sysUser){
        System.out.println(sysUser);
        sysService.addSysUser(sysUser);
        return "添加成功";
    }

    @GetMapping("/toXtiper")
    public String toXtiper(){
        return "xtiper";
    }

//    /**
//     *
//     * @return
//     */
//    @GetMapping("/login/invalid")
//    @ResponseStatus(HttpStatus.UNAUTHORIZED)
//    @ResponseBody
//    public String invalid() {
//        return "Session会话 已过期，请重新登录";
//    }
//
//    /**
//     * 主动踢出用户
//     * @param username
//     * @return
//     */
//    @GetMapping("/kick")
//    @ResponseBody
//    public String removeUserSessionByUsername(@RequestParam String username) {
//        System.out.println("要踢出的用户是："+username);
//        int count = 0;
//
//        // 获取session中所有的用户信息
//        List<Object> users = sessionRegistry.getAllPrincipals();
//        for (Object principal : users) {
//            if (principal instanceof User) {
//                String principalName = ((User)principal).getUsername();
//                System.out.println("遍历用户："+principalName);
//                if (principalName.equals(username)) {
//                    // 参数二：是否包含过期的Session
//                    List<SessionInformation> sessionsInfo = sessionRegistry.getAllSessions(principal, false);
//                    if (null != sessionsInfo && sessionsInfo.size() > 0) {
//                        for (SessionInformation sessionInformation : sessionsInfo) {
//                            sessionInformation.expireNow();
//                            count++;
//                        }
//                    }
//                }
//            }
//        }
//        return "操作成功，清理session共" + count + "个";
//    }







}
