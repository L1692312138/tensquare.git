package com.lsh.controller;

import com.lsh.model.User;
import com.lsh.service.UserService;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 8:59 下午
 * @desc ：
 */
//@RefreshScope
@RestController
@RequestMapping("/tensquareUser")
public class UserController {
    @Autowired
    UserService service;
    @GetMapping("/findAll")
    public ResultObject findAll(){
        List<User> list  = service.findAll();
        return new ResultObject(true, StatusCode.OK,"查询成功",list);
    }
}
