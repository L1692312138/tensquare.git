package com.lsh;

import com.lsh.model.SysRolePermission;
import com.lsh.repository.SysRolePermissionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/10 10:14 下午
 * @desc ：
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class JPATest {
    @Autowired
    SysRolePermissionRepository sysRolePermissionRepository;
    @Test
    public void test1(){
        List<SysRolePermission> sysRolePermissionByRoleId = sysRolePermissionRepository.findSysRolePermissionByRoleId(1);
        System.out.println(sysRolePermissionByRoleId);

    }
}
