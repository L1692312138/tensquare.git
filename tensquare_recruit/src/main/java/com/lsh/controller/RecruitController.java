package com.lsh.controller;

import com.lsh.service.RecruitService;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:32 上午
 * @desc ：
 */
@RefreshScope
@RestController
@RequestMapping("/recruit")
public class RecruitController {

    @Autowired
    RecruitService service;

    // 推荐职位
    @GetMapping("/search/recommend")
    public ResultObject recommend() {
        return new ResultObject(true, StatusCode.OK,"查询成功",service.findTop2ByStateOrderByCreatetimeDesc());
    }

    // 最新职位
    @GetMapping("/search/newlist")
    public ResultObject newJob() {
        return new ResultObject(true, StatusCode.OK,"查询成功",service.newJob());
    }

}

