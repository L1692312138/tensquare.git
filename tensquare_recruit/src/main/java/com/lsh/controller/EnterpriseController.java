package com.lsh.controller;

import com.lsh.service.EnterpriseService;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:17 上午
 * @desc ：
 */
@RefreshScope
@RestController
@RequestMapping("/enterprise")
public class EnterpriseController {
    @Autowired
    EnterpriseService service;
    @GetMapping("/search/hotlist")
    public ResultObject findByIsHot() {
        return new ResultObject(true, StatusCode.OK,"查询成功",service.findByIshot("1"));
    }
}