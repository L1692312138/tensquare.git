package com.lsh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:17 上午
 * @desc ：
 */
@EnableEurekaClient
@SpringBootApplication
public class RecruitApplicatiion {
    public static void main(String[] args) {
        SpringApplication.run(RecruitApplicatiion.class);
    }
}
