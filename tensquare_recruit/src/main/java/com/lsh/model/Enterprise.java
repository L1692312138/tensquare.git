package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:19 上午
 * @desc ：
 */
@Data
@Entity
@Table(name = "tb_enterprise")
public class Enterprise {

    /**
     *  id
     */
    @Id
    private String id;
    /**
     *  公司名
     */
    private String name;
    /**
     *  公司简介
     */
    private String summary;
    /**
     *  公司地址
     */
    private String address;
    /**
     *  公司所有标签
     */
    private String labels;
    /**
     *  坐标
     */
    private String coordinate;
    /**
     *  是否热门
     */
    private String ishot;
    /**
     *  logo
     */
    private String logo;
    /**
     *  职位数
     */
    private String jobcount;
    /**
     *  url
     */
    private String url;
}

