package com.lsh.repository;

import com.lsh.model.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:22 上午
 * @desc ：
 */
public interface EnterpriseRepository extends JpaRepository<Enterprise,String>, JpaSpecificationExecutor<Enterprise> {
    /**
     * 是否热门
     * @param isHot
     * @return
     */
    List<Enterprise> findByIshot(String isHot);
}
