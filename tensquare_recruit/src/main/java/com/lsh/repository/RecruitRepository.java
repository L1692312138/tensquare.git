package com.lsh.repository;

import com.lsh.model.Recruit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:28 上午
 * @desc ：
 */
public interface RecruitRepository extends JpaRepository<Recruit,String>, JpaSpecificationExecutor<Recruit> {

    /**
     * 推荐职位   为2,根据Createtime倒序,取前2条数据
     * @param state
     * @return
     */
    List<Recruit> findTop2ByStateOrderByCreatetimeDesc(String state);

    /**
     * 最新职位 : state不为0, 根据Createtime倒序,取前2数据
     * @param state
     * @return
     */
    List<Recruit> findTop2ByStateNotOrderByCreatetimeDesc(String state);


}

