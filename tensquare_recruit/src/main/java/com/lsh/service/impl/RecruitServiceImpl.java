package com.lsh.service.impl;

import com.lsh.model.Recruit;
import com.lsh.repository.RecruitRepository;
import com.lsh.service.RecruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:30 上午
 * @desc ：
 */
@Service
public class RecruitServiceImpl implements RecruitService {
    @Autowired
    RecruitRepository repository;


    /**
     * 推荐职位
     * @return
     */
    @Override
    public List<Recruit> findTop2ByStateOrderByCreatetimeDesc() {
        return repository.findTop2ByStateOrderByCreatetimeDesc("2");
    }

    /**
     * 最新职位
     */
    @Override
    public List<Recruit> newJob() {
        return repository.findTop2ByStateNotOrderByCreatetimeDesc("0");
    }

}
