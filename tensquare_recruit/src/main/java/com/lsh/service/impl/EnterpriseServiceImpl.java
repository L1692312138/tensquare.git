package com.lsh.service.impl;

import com.lsh.model.Enterprise;
import com.lsh.repository.EnterpriseRepository;
import com.lsh.service.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:23 上午
 * @desc ：
 */
@Service
public class EnterpriseServiceImpl implements EnterpriseService {
    @Autowired
    EnterpriseRepository repository;

    @Override
    public List<Enterprise> findByIshot(String isHot) {
        return repository.findByIshot(isHot);
    }
}
