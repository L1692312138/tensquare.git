package com.lsh;

import com.lsh.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:33 上午
 * @desc ：
 */
@EnableEurekaClient
@SpringBootApplication
public class QAApplication {
    public static void main(String[] args) {
        SpringApplication.run(QAApplication.class);
    }
    @Bean
    public IdWorker getIdWorker(){
        return new IdWorker(1,1);
    }
}
