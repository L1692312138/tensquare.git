package com.lsh.service.impl;

import com.lsh.model.Problem;
import com.lsh.repository.ProblemRepository;
import com.lsh.service.ProblemService;
import com.lsh.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:44 上午
 * @desc ：
 */
@Service
public class ProblemServiceImpl implements ProblemService {
    @Autowired
    private ProblemRepository repository;

    @Autowired
    private IdWorker idWorker;

    @Override
    public Page<Problem> newlist(String labelid, int page, int rows){
        Pageable pageable = PageRequest.of(page-1, rows);
        return repository.newlist(labelid, pageable);
    }
    @Override
    public Page<Problem> hotlist(String labelid, int page, int rows){
        Pageable pageable = PageRequest.of(page-1, rows);
        return repository.hotlist(labelid, pageable);
    }
    @Override
    public Page<Problem> waitlist(String labelid, int page, int rows){
        Pageable pageable = PageRequest.of(page-1, rows);
        return repository.waitlist(labelid, pageable);
    }

}
