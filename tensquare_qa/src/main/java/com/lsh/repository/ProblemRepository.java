package com.lsh.repository;

import com.lsh.model.Problem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:41 上午
 * @desc ：
 */
public interface ProblemRepository extends JpaRepository<Problem,String>, JpaSpecificationExecutor<Problem> {

    /**
     * 最新回答列表 :
     * Pageable pageable 用于分页
     * 有nativeQuery = true时，是可以执行原生sql语句
     * 没有nativeQuery = true 是以jpql 的形式执行,表名是类名,字段是属性
     * 返回的Page是SpringData提供的
     * @param labelid
     * @param pageable
     * @return
     */
    @Query(value = "SELECT * FROM tb_problem, tb_pl WHERE id = problemid AND labelid=? ORDER BY replytime DESC", nativeQuery = true)
    public Page<Problem> newlist(String labelid, Pageable pageable);


    /**
     * 热门回答列表
     * @param labelid
     * @param pageable
     * @return
     */
    @Query(value = "SELECT * FROM tb_problem, tb_pl WHERE id = problemid AND labelid=? ORDER BY reply DESC", nativeQuery = true)
    public Page<Problem> hotlist(String labelid, Pageable pageable);


    /**
     * 等待回答列表
     * @param labelid
     * @param pageable
     * @return
     */
    @Query(value = "SELECT * FROM tb_problem, tb_pl WHERE id = problemid AND labelid=? AND reply=0 ORDER BY createtime DESC", nativeQuery = true)
    public Page<Problem> waitlist(String labelid, Pageable pageable);
}

