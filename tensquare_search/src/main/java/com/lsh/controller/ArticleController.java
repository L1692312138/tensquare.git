package com.lsh.controller;

import com.lsh.model.ESArticle;
import com.lsh.service.ArticleService;
import com.lsh.util.PageResult;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/30 10:18 上午
 * @desc ：
 */
@RefreshScope
@RestController
@RequestMapping("/article")
@CrossOrigin
public class ArticleController {
    @Autowired
    ArticleService articleService;

    @GetMapping
    public ResultObject findAll(){
        List<ESArticle> lists = articleService.findAll();
        return new ResultObject(true, StatusCode.OK, "查询所有成功",lists);
    }

    @PostMapping
    public ResultObject save(@RequestBody ESArticle article) {
        articleService.save(article);
        return new ResultObject(true, StatusCode.OK, "添加成功");
    }

    @GetMapping(value = "/{key}/{page}/{size}")
    public ResultObject findByKey(@PathVariable String key, @PathVariable int page, @PathVariable int size){
        if ("null".equals(key)){
            key = null;
        }
        Page<ESArticle> pageData = articleService.findByKey(key, page, size);
        return new ResultObject(true, StatusCode.OK, "查询成功", new PageResult<ESArticle>(pageData.getTotalElements(), pageData.getContent()));
    }

}

