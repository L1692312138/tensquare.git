package com.lsh.repository;

import com.lsh.model.ESArticle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/30 10:16 上午
 * @desc ：
 */
public interface ArticleRepository extends ElasticsearchRepository<ESArticle, String> {

    /**
     * 添加如下自定义方法
     * @param title
     * @param content
     * @param pageable
     * @return
     */
    public Page<ESArticle> findByTitleOrContentLike(String title, String content, Pageable pageable);
}
