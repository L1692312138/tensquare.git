package com.lsh.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/4 10:45 上午
 * @desc ：前台服务可能会在头中存放一些信息,让其全部放行即可.此处假设请求头中带了一个叫做” Authorization”的头信息.
 */
@Slf4j
@Component
public class WebZuulFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        // 是否需要拦截: true需要拦击  false 不拦截
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String uri = request.getRequestURI();
        log.info("请求的uri : "+uri);
        if (uri.contains("/user/login")){
            // 放行,不拦截,不执行run方法
            return false;
        }
        // 其余所有都拦截,执行run方法
        return true;
    }

    /*
     * 拦截之后做的事情
     */

    @Override
    public Object run() throws ZuulException {
        //得到request上下文
        RequestContext ctx  = RequestContext.getCurrentContext();
        //得到request域
        HttpServletRequest request = ctx .getRequest();
        //得到头信息
        String header = request.getHeader("Authorization");
        //判断是否有头信息
        if(header!=null && !"".equals(header)){
            log.info("Authorization："+header);
            //把头信息继续向下传
            ctx .addZuulRequestHeader("Authorization", header);
        }

        String uri = request.getRequestURI();
        log.info("ZUUL MyFilter run()执行,此时拦截的路径是 : "+uri);
        /*
         * 此处可以通过request对象获得请求中的数据,比如session,cookie,parameter等,
         * 进行判断 ,
         * 如果是预期的结果 就放行[返回任意数据 : 源码说的]即可
         * 如果不是预期的结果,就以抛出异常的形式不放行
         */
        /*
         * 此处演示:假如是删除数据请求请求,直接返回异常
         */
        if("deleteAllData/**".equals(uri)){
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(404);
            try {
                HttpServletResponse resp =  ctx.getResponse();
                resp.setContentType("text/html; charset=utf-8");
                resp .getWriter().write("请求错误");
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        return null;
    }
}

