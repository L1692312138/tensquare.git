package com.lsh.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/3 5:31 下午
 * @desc ：
 */
@Component
@FeignClient("tensquare-user")
public interface UserClient {

    @PutMapping(value = "/user/{userid}/{friendid}/{x}")
    void updateFansCountAndFollowCount(@PathVariable("userid") String userid, @PathVariable("friendid") String friendid, @PathVariable("x") int x);

}

