package com.lsh.controller;

import com.lsh.model.Spit;
import com.lsh.service.SpitService;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:53 下午
 * @desc ：
 */
@RefreshScope
@RestController
@CrossOrigin
@RequestMapping("/spit")
public class SpitController {

    @Autowired
    SpitService spitService;



    @GetMapping
    public ResultObject findAll(){
        System.out.println("Controller_findAll");
        return new ResultObject(true, StatusCode.OK, "查询成功", spitService.findAll());
    }

    @GetMapping(value = "/{spitId}")
    public ResultObject findById(@PathVariable String spitId){
        return new ResultObject(true, StatusCode.OK, "查询成功", spitService.findById(spitId));
    }

    @PostMapping
    public ResultObject save(@RequestBody Spit spit){
        spitService.save(spit);
        return new ResultObject(true, StatusCode.OK, "保存成功");
    }

    @PutMapping(value = "/{spitId}")
    public ResultObject update(@PathVariable String spitId, @RequestBody Spit spit){
        spit.set_id(spitId);
        spitService.update(spit);
        return new ResultObject(true, StatusCode.OK, "修改成功");
    }

    @DeleteMapping(value = "/{spitId}")
    public ResultObject delete(@PathVariable String spitId){
        spitService.deleteById(spitId);
        return new ResultObject(true, StatusCode.OK, "删除成功");
    }

    @PutMapping("/inc/{spitId}")
    public ResultObject inc(@PathVariable String spitId){
        spitService.incVisist(spitId);
        return new ResultObject(true, StatusCode.OK, "增加成功");

    }

}

