package com.lsh.repository;

import com.lsh.model.Spit;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:57 下午
 * @desc ：类似于JPA的操作一样,创建接口继承一个MongoRepository,就可以使用默认的增删改查的操作
 */
public interface SpitRepository extends MongoRepository<Spit, String> {
}
