package com.lsh;

import com.lsh.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:52 下午
 * @desc ：(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
 */
@EnableEurekaClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SpitApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpitApplication.class);
    }

    @Bean
    public IdWorker getIdWorkder() {
        return new IdWorker(1,1);
    }
}
