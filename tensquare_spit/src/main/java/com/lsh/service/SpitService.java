package com.lsh.service;

import com.lsh.model.Spit;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 2:53 下午
 * @desc ：
 */
public interface SpitService {
    /**
     * 查询全部数据
     * @return
     */
    List<Spit> findAll();
    /**
     * 根据id查询
     * @return
     */
    Spit findById(String id);
    /**
     * 添加数据
     * @return
     */
    void save(Spit spit);
    /**
     * 更新数据
     * @return
     */
    void update(Spit spit);
    /**
     * 删除数据
     * @return
     */
    void deleteById(String id);

    void incVisist(String spitId);
}
