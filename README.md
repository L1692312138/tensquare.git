# 架构+技术
前后端分离<br>
前后台交互，用restful接口文档<br>
项目架构  用微服务技术每个微服务最终打包成docker镜像<br>
技术选型  spring全家桶，SpringBoot Spring data spring cloud<br>
# 项目介绍
程序员交友,讨论的网站.
# 开发环境准备
工具:IDEA + VMware + Navicat
技术:
	SpringBoot+SpringData+SpringCloud<br>
	Docker   确保VMware中已经安装docker<br>
# 各个微服务的端口号
eureka  注册中心：6868<br>
base    基础模块：9001 <br>
user 用户模块：9002<br>
recruit 招聘微服务模块：9003<br>
qa      问答模块：9004<br>
friend      交友模块：9005<br>
common  公共模块：9006<br>
spit 吐槽模块：9007<br>
search 搜索模块：9008<br>
manager 后台网关模块：9009<br>



