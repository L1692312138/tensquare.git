package com.lsh.service;

import com.lsh.model.Article;
import org.springframework.data.domain.Page;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:58 上午
 * @desc ：
 */
public interface ArticleService {
    void updateState(String id);

    void addThumbup(String id);

    Article findById(String id);

    void add(Article article);

    Page<Article> search(Article article, int pageNum, int pageSize);
}
