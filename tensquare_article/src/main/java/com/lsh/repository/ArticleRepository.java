package com.lsh.repository;

import com.lsh.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/29 9:57 上午
 * @desc ：
 */
public interface ArticleRepository extends JpaRepository<Article,String>, JpaSpecificationExecutor<Article> {

    /**
     * 不加报错,JPA 默认操作,增删改时使用, 在调用的地方必须加事务，没有事务不能正常执行
     * @param id
     */
    @Modifying
    @Query(value = "UPDATE tb_article SET state=1 WHERE id = ?", nativeQuery = true)
    public void updateState(String id);

    @Modifying
    @Query(value = "UPDATE tb_article SET thumbup=thumbup+1 WHERE id = ?", nativeQuery = true)
    public void addThumbup(String id);
}

