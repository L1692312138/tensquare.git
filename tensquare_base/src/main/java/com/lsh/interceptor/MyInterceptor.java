package com.lsh.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 2:10 下午
 * @desc ：
 */
// 该注解使类被Spring容器托管
@Component
public class MyInterceptor implements HandlerInterceptor {

    /**
     * 重新方法: 拦截请求,获得登录信息
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("MyInterceptor -url= "+request.getRequestURL());
        return true;
    }
}

