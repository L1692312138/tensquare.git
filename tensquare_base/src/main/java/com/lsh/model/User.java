package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tb_user")
public class User {
    /**
     * 因为字段与数据库一致 所以没有加注解
     * 如果属性与字段不一致 需要使用@Column
     */
    @Id
    private String id;
    /**
     * 登录名
     */
    private String loginname;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String sex;
    /**
     * 生日
     */
    private String birthday;

}
