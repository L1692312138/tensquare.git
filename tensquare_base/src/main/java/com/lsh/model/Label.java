package com.lsh.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tb_label")
public class Label {
    /**
     * 因为字段与数据库一致 所以没有加注解
     * 如果属性与字段不一致 需要使用@Column
     */
    @Id
    private String id;
    /**
     * 标签名称
     */
    private String labelname;
    /**
     * 状态
     */
    private String state;
    /**
     * 使用数量
     */
    private Integer count;
    /**
     * 是否推荐
     */
    private String recommend;
    private String fans;

}
