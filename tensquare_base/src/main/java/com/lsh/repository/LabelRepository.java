package com.lsh.repository;

import com.lsh.model.Label;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 9:03 下午
 * @desc ：
 */
public interface LabelRepository extends JpaRepository<Label,String>, JpaSpecificationExecutor<Label> {
}
