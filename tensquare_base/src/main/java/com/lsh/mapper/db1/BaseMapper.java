package com.lsh.mapper.db1;

import com.lsh.model.Label;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 2:22 下午
 * @desc ：
 */
//该注解说明该类就行相当于一个xxxMapper.xml文件
@Mapper
public interface BaseMapper {

    List<Label> findAllLabelByMapperXML();

    @Select("SELECT * FROM `tb_label`")
    List<Label> findAll();

    @Select("SELECT * FROM `tb_label` WHERE `id` = #{id}")
    Label findById(String id);

    /**
     * 模糊查询 -
     * 因为#{} 取值出会自带引号,${}取值就是去除原值,当参数是基本数据类型,其中必须是value
     * @param labelname
     * @return
     */
    @Select("SELECT * FROM `tb_label` WHERE `labelname` LIKE '%${value}%'")
    List<Label> findByKeyword(String labelname);

    /**
     * Mapper接口方法中多个参数,想要注入到SQL语句中,需要使用注解@Param
     * @param labelname
     * @param fans
     * @return
     */
    @Select("SELECT * FROM `tb_label` WHERE `labelname` LIKE '%${labelname}%' AND `fans` LIKE '%${fans}%'")
    List<Label> findByKeysords(@Param("labelname") String labelname,@Param("fans") String fans);

    /**
     * 添加
     * @param label
     */
    @Insert("INSERT INTO `tb_label`  VALUES (#{id},#{labelname},#{state},#{count},#{recommend},#{fans})")
    void addLabel (Label label);

    /**
     * 更新
     * @param label
     */
    @Update("UPDATE `tb_label` SET `labelname` = #{labelname} ,`state` = #{state},`count`=#{count},`recommend`=#{recommend},`fans`=#{fans} WHERE `id` = #{id}")
    void updateLabel(Label label);

    /**
     * 删除
     * @param id
     */
    @Delete("DELETE FROM `tb_label` WHERE `id` = #{id}")
    void deleteById(String id);

}
