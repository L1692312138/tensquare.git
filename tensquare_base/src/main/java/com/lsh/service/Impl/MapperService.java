package com.lsh.service.Impl;

import com.lsh.mapper.db1.BaseMapper;
import com.lsh.mapper.db2.UserMapper;
import com.lsh.model.Label;
import com.lsh.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 3:07 下午
 * @desc ：
 */
@Service
public class MapperService {
    @Autowired
    BaseMapper baseMapper;
    @Autowired
    UserMapper userMapper;
    public List<Label> findLabelByMapperXml(){
        return baseMapper.findAllLabelByMapperXML();
    }
    public List<User> findUserByMapperXml(){
        return userMapper.findAllLabelByMapperXML();
    }

    public List<Label> findAll(){
        return baseMapper.findAll();
    }
    public Label findById(String id){
        return baseMapper.findById(id);
    }
    public List<Label> findByKeyword(String  label){
        return baseMapper.findByKeyword(label);
    }
    public List<Label> findByKeywords(Label  label){
        return baseMapper.findByKeysords(label.getLabelname(),label.getFans());
    }
    public void add(Label  label){
         baseMapper.addLabel(label);
    }
    public void upadte(Label  label){
         baseMapper.updateLabel(label);
    }
    public void deleteById(String  id){
        baseMapper.deleteById(id);
    }


}
