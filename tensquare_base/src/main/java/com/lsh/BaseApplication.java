package com.lsh;

import com.lsh.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 8:36 下午
 * @desc ：
 */
@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication
public class BaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(BaseApplication.class);
    }

    /**
     *  注入雪花算法
     * @return
     */
    @Bean
    public IdWorker getIdWorkder() {
        return new IdWorker(1,1);
    }
}
