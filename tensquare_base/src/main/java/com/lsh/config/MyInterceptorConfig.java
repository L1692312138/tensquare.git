package com.lsh.config;

import com.lsh.interceptor.MyInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/5 2:12 下午
 * @desc ：
 */
// 该注解说明该类是一个Spring的配置类,相当于是之前的配置文件
@Configuration
public class MyInterceptorConfig implements WebMvcConfigurer {

    /**
     * 注意实现了接口
     * 重新添加拦截路径的方法
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 将自己的拦截器注册进运行环境中
        InterceptorRegistration registration = registry.addInterceptor(new MyInterceptor());
        // /* 匹配单层路径 /a 或者/b 可以拦截到 ,/a/b/c 这样的多层路径不会拦截
        // /** 匹配单层路径 /a 或者/b 多层路径/a/b/c 都可以
        // 支持后缀匹配.注意不是*.do 而是 /*.do
        // 支持路径中的模糊匹配 /user/**
        registration.addPathPatterns("/base/**");
        // 添加一些不拦截路径
        registration.excludePathPatterns("/login/**","/logout/**","/html/t1.html");
    }
}

