package com.lsh.controller;

import com.lsh.model.Label;
import com.lsh.service.LabelService;
import com.lsh.util.PageResult;
import com.lsh.util.ResultObject;
import com.lsh.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/28 8:59 下午
 * @desc ：
 */
@RefreshScope
@RestController
@RequestMapping("/base")
public class BaseController {
    @Autowired
    LabelService labelService;
    @Value("${SpringCloud.Config.value}")
    public String value;

    @GetMapping("/springCloudBusGetvalue")
    public ResultObject getValue(){
        return new ResultObject(true, StatusCode.OK,"消息总线",value);
    }

    @GetMapping
    public ResultObject findAll(){
        List<Label> list  = labelService.findAll();
        return new ResultObject(true, StatusCode.OK,"查询成功",list);
    }

    /**
     * 根据Id查标签
     */
    @GetMapping("/{labelId}")
    public ResultObject findById(@PathVariable String labelId) {
        Label label = labelService.findById(labelId);
        return new ResultObject(true, StatusCode.OK,"查询成功",label);
    }

    /**
     * 增加标签
     */
    @PostMapping
    public ResultObject add(@RequestBody Label label) {
        labelService.add(label);
        return new ResultObject(true, StatusCode.OK,"增加成功");
    }

    /**
     * 修改标签
     */
    @PutMapping("/{labelId}")
    public ResultObject update(@PathVariable String labelId,@RequestBody Label label) {
        label.setId(labelId);
        labelService.update(label);
        return new ResultObject(true, StatusCode.OK,"修改成功");
    }


    /**
     * 修改标签
     */
    @DeleteMapping("/{labelId}")
    public ResultObject deleteById(@PathVariable String labelId) {
        labelService.deleteById(labelId);
        return new ResultObject(true, StatusCode.OK,"删除成功");
    }

    /**
     * 根据条件查询
     */
    @PostMapping("/search")
    public ResultObject findSearch(@RequestBody Label label) {
        List<Label> lists = labelService.findSearch(label);
        return new ResultObject(true, StatusCode.OK,"查询成功",lists);
    }

    /**
     *  有条件的分页查询
     */
    @PostMapping("/search/{page}/{size}")
    public ResultObject findSearchPage(@RequestBody Label label,@PathVariable int page,@PathVariable int size) {
        // org.springframework.data.domain.Page SpringData的分页类,类似于Pagehelper的Pageinfo
        Page pageData = labelService.findSearchPage(label,page,size);
        // int getTotalElements() 获得总记录数
        // List getContent()       获得数据
        return new ResultObject(true, StatusCode.OK,"查询成功",new PageResult<Label>(pageData.getTotalElements(),pageData.getContent()));
    }





}
